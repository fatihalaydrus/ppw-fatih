from .views import profile, exp
from django.urls import path, include

urlpatterns = [
    path('', profile, name='profile'),
    path('exp/', exp, name='exp'),
]
