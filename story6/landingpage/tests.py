from django.test import TestCase
from django.apps import apps
from .apps import LandingpageConfig

# Create your tests here.
class UnitTest(TestCase):
    def test_landingpage_app(self):
        self.assertEqual(LandingpageConfig.name, 'landingpage')
        self.assertEqual(apps.get_app_config('landingpage').name, 'landingpage')
    