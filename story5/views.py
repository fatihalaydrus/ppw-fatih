from django.shortcuts import render, redirect
from .forms import AddSchedule
from .models import Schedule
from django.utils import timezone

# Create your views here.
def add_schedule(request):
    form = AddSchedule()
    if request.method == "POST":
        form = AddSchedule(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')

    response = {
        'title' : 'Add Activity',
        'form' : form,
        }
    return render(request, 'story5/add_activity.html', response)

def schedule(request):
    schedules = Schedule.objects.order_by("datetime")
    response = {
        'title' : 'Schedule',
        'schedules' : schedules,
        'server_time' : timezone.now(),
        }
    return render(request, 'story5/schedule.html', response)

def delete_schedule(request):
    if request.method == "POST":
        id = request.POST['id']
        Schedule.objects.get(id=id).delete()
    
    return redirect('schedule')