from django import forms
from .models import Schedule

class AddSchedule(forms.ModelForm):
    activity = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "required" : True,
        "placeholder" : "Activity Name",
    }))
    category = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "required" : False,
        "placeholder" : "Category",
    }))
    datetime = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control datetimepicker-input',
            'placeholder' : 'Activity Datetime',
            'data-target': '#datetimepicker1'
        })
    )
    location = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "required" : False,
        "placeholder" : "Location",
    }))

    class Meta:
        model = Schedule
        fields = ['activity', 'category', 'datetime', 'location']