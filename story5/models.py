from django.db import models
from django.utils import timezone

# Create your models here.
class Schedule(models.Model):
    activity = models.CharField(max_length=64)
    category = models.CharField(max_length=24)
    datetime = models.DateTimeField(default=timezone.now())
    location = models.CharField(max_length=64)
    
    def __str__(self):
        return self.activity