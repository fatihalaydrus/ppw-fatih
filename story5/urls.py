from .views import *
from django.urls import path

urlpatterns = [
    path('add/', add_schedule, name='add_schedule'),
    path('', schedule, name='schedule'),
    path('delete/', delete_schedule, name='delete_schedule'),
]
